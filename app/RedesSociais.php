<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedesSociais extends Model
{
    //

    protected $fillable = ['name', 'link', 'id'];
}
