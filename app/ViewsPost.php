<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ViewsPost extends Model
{
    //
	protected $fillable = [
		'post_id',
		'created_at',
		'updated_at'
	];

	public function post(){
        return $this->belongsTo('App\Post');
    }
	
}
