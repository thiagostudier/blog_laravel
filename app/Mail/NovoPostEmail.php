<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Post;

class NovoPostEmail extends Mailable
{
    use Queueable, SerializesModels;   

    /**
     * Create a new message instance.
     *
     * @return void
     */ 

    public $post;
    public $quemsomos;
    public $quemsomos_logo;

    public function __construct(Post $post, $quemsomos, $quemsomos_logo)
    {
        //
        $this->post = $post;
        $this->quemsomos = $quemsomos;
        $this->quemsomos_logo = $quemsomos_logo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */

    public function build()
    {   
        return $this->from('thiago.studier9@gmail.com', 'Thiago')
            ->subject($this->quemsomos . ' acabou de publicar um novo conteúdo')
            ->view('post_email');

    }
}

?>

