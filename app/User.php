<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'verifyToken', 'name', 'email', 'password', 'role_id', 'is_active', 'photo_id','facebook','twitter','instagram','pinterest','about',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\Role');
    }

    public function photo(){
        return $this->belongsTo('App\Photo');
    }

    public function post(){
        return $this->hasMany('App\Post');
    }
    
    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likes_posts(){
        return $this->belongsToMany('App\Post');
    }

    //Se o role for Admin
    public function isAdmin(){
        if(isset($this->role->name) and $this->role->name == 'Admin' and $this->is_active == 1){
            return true;
        }else{
            return false;
        }
    }

    public function isInscrito(){
        if(isset($this->role->name) and $this->role->name == 'Inscrito' and $this->is_active == 1){
            return true;
        }else{
            return false;
        }
    }

}
