<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    //

    protected $fillable = [
    	'photo_id',
    	'category_id',
    	'user_id',
    	'title',
        'body',
        'is_active',
        'destaque_aside',
    	'destaque_home'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function photo(){
    	return $this->belongsTo('App\Photo');
    }

    public function comments(){
        return $this->hasMany('App\Comment');
    }

    public function likes_users(){
        return $this->belongsToMany('App\User');
    }

    public function views_post(){
        return $this->hasMany('App\ViewsPost');
    }

}
