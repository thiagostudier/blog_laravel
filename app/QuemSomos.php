<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuemSomos extends Model
{
    //
    protected $fillable = [
        'name', 'historia', 'objetivo', 'email', 'celular', 'telefone', 'lema', 'logo', 'banner', 'facebook_page', 'twitter_page', 'pinterest_page'
    ];

    protected $uploads = "\images/";

    public function getLogoAttribute($logo){
		return $this->uploads . $logo;
	}

	public function getBannerAttribute($banner){
		return $this->uploads . $banner;
	}

    public function photo(){
    	return $this->belongsTo('App\Photo');
    }

}
