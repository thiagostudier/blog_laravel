<?php

use App\User;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('verify/{email}/{verifyToken}','Auth\RegisterController@sendEmailDone')->name('sendEmailDone');

Route::get('verify/{email}/{verifyToken}',function($email, $verifyToken){

	$user = User::where('email',$email)->first();
        
    if($user){
        user::where('email',$email)->update(['is_active'=>'1', 'verifyToken'=>null]);

        Session::flash('success_validar_email', 'Conta ativada com sucesso!');

        return redirect('/');

    }else{
        return 'erro';
    }
})->name('sendEmailDone');

Route::auth();

Route::get('/', 'IndexController@index');

Route::get('/home', 'IndexController@index');

Route::middleware(['inscrito'])->group(function () {
    Route::get('user', 'UserController@index');
    Route::get('user/edit', 'UserController@edit');
    Route::get('user/update', 'UserController@update');
    Route::resource('user', 'UserController');
});

Route::get('/logout', 'Auth\LoginController@logout');

Route::group(['middleware'=>'admin'], function(){
	
	Route::get('/admin', function(){
		return view('admin.index'); 
	});

	Route::resource('admin/users', 'AdminUsersController');


	Route::resource('admin/redessociais', 'RedesSociaisController');

	Route::resource('admin/posts', 'AdminPostsController');

	Route::resource('admin/categories', 'AdminCategoriesController');

	Route::resource('admin/comments', 'PostCommentsController');

	Route::resource('admin/comment/replies', 'CommentRepliesController');

	Route::resource('admin/quemsomos', 'QuemSomosController');

});

Route::get('/post/{id}', ['as'=>'home.post', 'uses'=>'AdminPostsController@post']); // as nomeia a rota

Route::get('send', 'ContatoController@send');

Route::get('fale_conosco',function(){

	return view('fale_conosco');
});

Route::post('enviar', 'ContatoController@enviaContato');

Route::get('/curtir_post', ['as'=>'curtir_post', 'uses'=>'UserController@curtir']); // as nomeia a rota

Route::get('/descurtir_post', ['as'=>'descurtir_post', 'uses'=>'UserController@descurtir']); // as nomeia a rota

Route::resource('comments', 'PostCommentsController');

Route::resource('replies', 'CommentRepliesController');

Route::resource('comments', 'PostCommentsController');

Route::get('/category/{id}', ['as'=>'category', 'uses'=>'IndexController@categories']); // as nomeia a rota

Route::get('/quemsomos', ['as'=>'quemsomos', 'uses'=>'IndexController@index_quemsomos']); // as nomeia a rota

Route::get('search_ajax_post', 'IndexController@search_ajax_post');

Route::get('/search_post/', ['as'=>'search_post','uses'=>'IndexController@search_post']);


// LIKES
// Route::post('curtir_post/{post}', ['uses' => 'UserController@curtir', 'as' => 'user_add_like'])