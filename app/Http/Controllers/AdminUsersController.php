<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UsersEditRequest;

use App\User;
use App\Role;
use App\Photo;

class AdminUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
        $users = User::paginate(2);

        if ($request->ajax()) {
            return view('admin.users.load', ['users' => $users])->render(); 
        }

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $roles = Role::pluck('name','id')->all();

        return view('admin.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UsersRequest $request)
    {
        //

        if(trim($request->password == '')) {

            $input = $request->except('password');

        }else{

            $input = $request->all();

            $input['password'] = bcrypt($request->password); 
        }

        if($file = $request->file('photo_id')) {            

            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            $photo = Photo::create(['file'=>$name]);

            $input['photo_id'] = $photo->id;

        }       

        User::create($input);

        Session::flash('success', 'Usuário criado com sucesso!');

        return redirect('/admin/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $roles = Role::pluck('name', 'id')->all();

        $user = User::findOrFail($id);

        return view('admin.users.edit', compact('user', 'roles'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UsersEditRequest $request, $id)
    {
        //

        $user = User::findOrFail($id);

        if( ($request->password == '') or ( bcrypt($request->password) == $user->password )){
            
            $input = $request->except('password');
               
        }else{

            $input = $request->all();

            $input['password'] = bcrypt($request->password); 
        }
        
        if($input['email'] != $user->email){
            $this->validate($request, [
                'email' => 'unique:users',
            ]);           
        }

        if($file = $request->file('photo_id')){ //coloca o file entro do request no $file

            $name = time() . $file->getClientOriginalName(); //cria o nome da imagem com a hora e o nome

            $file->move('images', $name); // move o $file dentro da pasta images

            $photo = Photo::create(['file'=>$name]); //insert na tabela Photo e coloca esse registro dentro de $photo

            $input['photo_id'] = $photo->id; //coloca a id do regitro acima dentro do input photo_id

        }

        $user->update($input);

        Session::flash('success', 'Usuário atualizado com sucesso!');

        return redirect('/admin/users');

    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // $user = User::destroy($id);

        // User::findOrFail($id)->delete();

        $user = User::findOrFail($id);

        $user->delete();

        Session::flash('success', 'The user has been deleted');

        return redirect('/admin/users');

    }
}
