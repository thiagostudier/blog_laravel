<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\OrderShipped;
use Illuminate\Support\Facades\Mail;
use App\Post;
use App\Photo;
use App\Category;
use App\QuemSomos;
use App\User;
use App\RedesSociais;

use App\Http\Requests\PostsCreateRequest;
use App\Http\Requests\PostsEditRequest;

use Illuminate\Support\Facades\Auth;

use App\Mail\ContatoEmail;
use App\Mail\NovoPostEmail;


class AdminPostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $posts = Post::limit(10)->orderBy('updated_at','desc')->paginate(10);
        $posts_aside = Post::where('destaque_aside', 1)->orderBy('updated_at','desc')->paginate(10);
        $post_header = Post::where('destaque_home', 1)->orderBy('updated_at','desc')->paginate(10);

        return view('admin/posts/index', compact('posts', 'posts_aside', 'post_header'));


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

        $posts_header = Post::where('destaque_home', 1)->get();

        $posts_header = $posts_header->count();

        $categories = Category::pluck('name', 'id')->all();

        return view('admin/posts/create', compact('categories', 'posts_header'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostsCreateRequest $request)
    {
        //

        $user = Auth::user(); //pegando o usuario logado

        $input = $request->all();

        if($file = $request->file('photo_id')){
            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            $photo = Photo::create(['file'=>$name]);

            $input['photo_id'] = $photo->id;

        }

        $input['user_id'] = $user->id;

        $users = User::where('is_active', '1')->select('email')->get();

        $quemsomos = QuemSomos::limit(1)->value('name');

        $quemsomos_logo = QuemSomos::limit(1)->value('logo');

        // return $quemsomos;
        // die();

        $post = Post::create($input);      

        Mail::to()->bcc($users)->send(new NovoPostEmail($post, $quemsomos, $quemsomos_logo));

        return redirect('/admin/posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $post = Post::findOrFail($id);

        $categories = Category::pluck('name', 'id')->all();

        return view('admin.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostsEditRequest $request, $id)
    {
        //

        $input = $request->all();

        if($file = $request->file('photo_id')){

            $name = time() . $file->getClientOriginalName();

            $file->move('images', $name);

            $photo = Photo::create(['file'=>$name]);

            $input['photo_id'] = $photo->id;

        }

        $post = Post::findOrFail($id);

        $post->update($input);

        return redirect('/admin/posts');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $post = Post::findOrFail($id);

        unlink(public_path() . "/images/" . $post->photo->file);

        $post->delete();

        return redirect('/admin/posts');
    }

    public function post($id){

        $post_view = Post::findOrFail($id);

        $comments = $post_view->comments()->whereIsActive(1)->get();

        return view('post', compact('post_view', 'comments'));
    }
}
