<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Redirector;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UsersEditRequest;

use App\Http\Requests\UserEditInscrito;

use App\Post;
use App\Category;
use App\QuemSomos;
use App\User;
use App\Photo;
use App\RedesSociais;


class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $posts_header = Post::where('is_active', 1)->where('destaque_home', 1)->orderBy('updated_at','desc')->get();

        $posts = Post::where('is_active', 1)->orderBy('created_at','desc')->offset(0)->limit(10)->get();   

        if ($request->ajax()) {
            return view('include.ajax.posts_home', ['posts' => $posts])->render();  
        }  

        return view('home', compact('posts', 'posts_header'));      
    }

    public function index_quemsomos(){  

        $empresa = QuemSomos::limit(1)->get();

        foreach($empresa as $nome){
            $nome_title = $nome->name;
        }  

        $admins = User::where('role_id', 1)->get();

        return view('quemsomos', compact('admins', 'empresa'));      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function user()
    {
        //
        // if(!Auth::user()){
        //     $posts_header = Post::where('is_active', 1)->where('destaque_home', 1)->orderBy('updated_at','desc')->get();

        //     $posts = Post::where('is_active', 1)->orderBy('created_at','desc')->paginate(7);

        //     return view('/home', compact('posts', 'posts_header'));      
        // }
        // $user = Auth::user();

        // return view('user.index', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //

        // $user = Auth::user();

        // return view('user.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditInscrito $request)
    {
        //

        // $user = Auth::user();

        // if( ($request->password == '') or ( bcrypt($request->password) == $user->password )){
            
        //     $input = $request->except('password');
               
        // }else{

        //     $input = $request->all();

        //     $input['password'] = bcrypt($request->password); 
        // }

        // if($input['email'] != $user->email){
        //     $this->validate($request, [
        //         'email' => 'unique:users',
        //     ]);           
        // }
        

        // if($file = $request->file('photo_id')){ //coloca o file entro do request no $file

        //     $name = time() . $file->getClientOriginalName(); //cria o nome da imagem com a hora e o nome

        //     $file->move('images', $name); // move o $file dentro da pasta images

        //     $photo = Photo::create(['file'=>$name]); //insert na tabela Photo e coloca esse registro dentro de $photo

        //     $input['photo_id'] = $photo->id; //coloca a id do regitro acima dentro do input photo_id

        // }

        // $user->update($input);

        // Session::flash('success', 'Usuário atualizado com sucesso!');

        // return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function categories(Request $request, $id){

        $category = Category::findOrFail($id);

        $posts_category = Post::where('category_id', $id)->where('is_active', 1)->orderBy('created_at','desc')->paginate(10);

        if ($request->ajax()) {
            return view('include.ajax.posts_category', ['posts_category' => $posts_category])->render();  
        }  

        return view('category', compact('posts_category', 'category')); 
    }

    public function search_post(Request $request){

        $pesquisa = $request->all();

        $pesquisa = $pesquisa['searchItem'];

        $input = $request->all();

        $posts_search = Post::where('title', 'like', '%'. $input['searchItem'] .'%')->orderBy('created_at','desc')->limit(20)->get();
        
        return view('search_post', compact('pesquisa', 'posts_search'));
        
    }

    public function search_ajax_post(Request $request){

        $input = $request->term;

        $resultados = Post::where('title', 'like', '%'. $input .'%')->get();

        if(count($resultados) == 0){
           $searchResult[] = 'Nenhum resultado encontrado'; 
        }else{
            foreach ($resultados as $value) {
                $searchResult[] = $value->title;
            }
        }

        return $searchResult;

    }

     public function more_posts(Request $request){

        $input = $request->all();

        $posts = Post::where('is_active', 1)->orderBy('created_at','desc')->offset($input['count'])->limit(5)->get();   

        $html = '';

        foreach ($posts as $post){     

            $html .= '<div class="col-md-12 item-article centralizar-verticalmente">';
                // $html .= '<a href="'.route('home.post', $post->id).'" class="col-md-6 item-photo-pequena item fundo-preto" >';
                //     $html .= '<img class="banner-post-home img-responsive" src="'.$post->photo->file.'">';
                // $html .= '</a>';
                // $html .= '<div class="col-md-6">';
                //     $html .= '<h5 class="media-heading">'.$post->category->name.'
                //             <small>'.$post->created_at->diffForHumans().' (<b>'.$post->created_at->formatLocalized('%d/%b/%y').'</b>)</small>
                //         </h5>';
                //     $html .= '<h2><b>'.$post->title.'</b></h2>';
                //     $html .= '<h4><small>Autor:'.$post->user->name.'</small></h4>';
                //     $html .= '<h4><small><a href="'.route('home.post', $post->id).'">Saiba mais</a></small></h4>';
                // $html .= '</div>';
            $html .= '</div>';                
            
        }

        if ($request->ajax()) {
            return $html;  
        }  

    }

}
