<?php
namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Mail;
use File;
use App\Photo;
use App\Mail\verifyEmail;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */
    use AuthenticatesUsers;
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function loginSocial(Request $request){

        // $socialType = $request->get('social_type');
        // \Session::put('social_type', $socialType);
        return \Socialite::driver('facebook')->redirect();

    }

    public function sendEmail($thisUser){
        Mail::to($thisUser['email'])->send(new verifyEmail($thisUser));
    }

    public function loginCallback(){

        // $socialType = \Session::pull('social_type');

        $userSocial = \Socialite::driver('facebook')->stateless()->user();

        //se o usuário não existir no meu banco
        $user = User::where('email', $userSocial->email)->first();

        if(!$user){

            $img = file_get_contents($userSocial->avatar);

            $name = time() . $userSocial->id . ".jpg";

            File::put("images/".$name, $img);

            $photo = Photo::create(['file'=>$name]); //insert na tabela Photo e coloca esse registro dentro de $photo

            $user = User::create([

                'email' => $userSocial->email,
                'password' => bcrypt(str_random(10)),
                'name' => $userSocial->name,
                'is_active' => 1,
                'verifyToken' => Str::random(40),
                'role_id' => 3,
                'photo_id' => $photo->id

            ]);

            \Auth::login($user);

        }else{
            
            \Auth::login($user);  
        }

        return redirect()->intended($this->redirectPath());

    }

}