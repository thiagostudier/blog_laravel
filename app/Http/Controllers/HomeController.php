<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

use App\Post;
use App\QuemSomos;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        foreach($empresa as $empresa){
            $nome_title = $empresa->name;
        } 

        $empresa = QuemSomos::limit(1)->get();

        $posts = Post::all();
        
        return view('home', compact('posts', 'nome_title', 'empresa'));
    }

    public function categories($id){
        $posts = Post::all()->whereCategoryId($id);

        return redirect('category', compact('posts'));
    }



}
