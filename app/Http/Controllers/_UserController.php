<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\Http\Requests\UserEditInscrito;

use App\Http\Requests;
use App\Http\Requests\UsersRequest;
use App\Http\Requests\UsersEditRequest;

use App\Post;
use App\Category;
use App\QuemSomos;
use App\User;
use App\Photo;
use App\RedesSociais;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $user = Auth::user();

        return view('user.index', compact('user'));      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //

        $user = Auth::user();

        return view('user.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserEditInscrito $request, $id)
    {
        //

        $user = Auth::user();

        if( ($request->password == '') or ( bcrypt($request->password) == $user->password )){
            
            $input = $request->except('password');
               
        }else{

            $input = $request->all();

            $input['password'] = bcrypt($request->password); 
        }

        if($input['email'] != $user->email){
            $this->validate($request, [
                'email' => 'unique:users',
            ]);           
        }
        

        if($file = $request->file('photo_id')){ //coloca o file entro do request no $file

            $name = time() . $file->getClientOriginalName(); //cria o nome da imagem com a hora e o nome

            $file->move('images', $name); // move o $file dentro da pasta images

            $photo = Photo::create(['file'=>$name]); //insert na tabela Photo e coloca esse registro dentro de $photo

            $input['photo_id'] = $photo->id; //coloca a id do regitro acima dentro do input photo_id

        }

        $user->update($input);

        Session::flash('success', 'Usuário atualizado com sucesso!');

        return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function curtir(Request $request){

        $user = Auth::user();

        $input = $request->all();

        $user->likes_posts()->attach($input);

        return redirect()->back();
        
    }

}
