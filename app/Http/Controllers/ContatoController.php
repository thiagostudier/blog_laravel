<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContatoEmail;
use Illuminate\Support\Facades\Session;

use App\QuemSomos;


class ContatoController extends Controller
{
    //
	public function enviaContato(Request $request){

		$email = QuemSomos::limit(1)->value('email');

		Mail::to($email)->send(new ContatoEmail($request));

		Session::flash('message_success', 'Mensagem enviada com sucesso!');

		return redirect()->back();
	}

}
