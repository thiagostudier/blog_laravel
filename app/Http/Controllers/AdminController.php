<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Post;
use App\ViewsPost;

class AdminController extends Controller
{
    //

	public function index(){

		$posts_famosos = Post::withCount('views_post')->orderByDesc('views_post_count')->limit(5)->get();

		$hoje = date("Y-m-d");

		$hoje_1 = date('Y-m-d', strtotime('-1 days', strtotime($hoje)));

		$hoje_2 = date('Y-m-d', strtotime('-2 days', strtotime($hoje)));

		$hoje_3 = date('Y-m-d', strtotime('-3 days', strtotime($hoje)));
		
		$hoje_4 = date('Y-m-d', strtotime('-4 days', strtotime($hoje)));

		$views_hoje = ViewsPost::where('created_at', 'like', '%'.$hoje.'%')->count();

		$views_hoje_1 = ViewsPost::where('created_at', 'like', '%'.$hoje_1.'%')->count();

		$views_hoje_2 = ViewsPost::where('created_at', 'like', '%'.$hoje_2.'%')->count();

		$views_hoje_3 = ViewsPost::where('created_at', 'like', '%'.$hoje_3.'%')->count();

		$views_hoje_4 = ViewsPost::where('created_at', 'like', '%'.$hoje_4.'%')->count();

		return view('admin.index', compact('posts_famosos', 'views_hoje', 'views_hoje_1', 'views_hoje_2', 'views_hoje_3', 'views_hoje_4'));
	}

}
