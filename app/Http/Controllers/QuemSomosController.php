<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\QuemSomos;

class QuemSomosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $quemsomos = QuemSomos::all();

        return view('admin.quemsomos.index', compact('quemsomos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //

        $quemsomos = QuemSomos::findOrFail($id);

        return view('admin.quemsomos.edit', compact('quemsomos'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $input = $request->all();

        if($logo = $request->file('logo')){
            $name = time() . $logo->getClientOriginalName();

            $logo->move('images', $name);

            $input['logo'] = $name;

        }

        if($banner = $request->file('banner')){
            $name = time() . $banner->getClientOriginalName();

            $banner->move('images', $name);

            $input['banner'] = $name;

        }

        QuemSomos::findOrFail($id)->update($input);

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
