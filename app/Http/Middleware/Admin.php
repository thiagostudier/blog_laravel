<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        //se estiver logado
        if(Auth::check()){
            //se estiver logado como admin - puxa o isAdmin dentro do model User
            if(Auth::user()->isAdmin()){
                return $next($request);
            }

        }
        // se a autentificação acima não for verificada como true, redirecionará para 404
        return redirect('/');

    }
}
