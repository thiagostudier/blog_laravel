<?php

namespace App\Http\Middleware;

use Closure;

class User
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(Auth::check()){
            //se estiver logado como admin - puxa o isAdmin dentro do model User
            if(Auth::user()->isInscrito() || Auth::user()->isAdmin()){
                return $next($request);
            }

        }
        return redirect('/');
    }
}
