@extends('layouts.blog-post')

@section('content')

	<h1>{{$category->name}}</h1>
	
		@if (count($posts_category) > 0)
            <section class="articles">
                @include('include.ajax.posts_category')
            </section>
        @endif
		
	</section>

	<script src="{{asset('js/libs.js')}}"></script>

    <script type="text/javascript">

        $(function() {
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();

                $('#load a').css('color', '#dfecf6');
                $('#load').append('Carregando...');

                var url = $(this).attr('href');
                getArticles(url);
                window.history.pushState("", "", url);
            });

            function getArticles(url) {
                $.ajax({
                    url : url
                }).done(function (data) {
                    $('.articles').html(data);
                }).fail(function () {
                    alert('Articles could not be loaded.');
                });
            }
        });
        
    </script>
		

@endsection