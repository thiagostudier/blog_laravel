@extends('layouts.blog-post')

@section('content')

    <?php use App\User; ?>

    <!-- Title -->
    <h1 class="titulo_post text-center">{{$post_view->title}}</h1>  

	<!-- Imagem post -->
    <br>
    <br>
    <img class="img-responsive banner-post" width="100%" src="{{$post_view->photo->file}}" alt="Image {{$post_view->title}}">      
    <br>
    <br>

    <!-- Post Content --> 
    <?php echo $post_view->body; ?>
    <br>
    <br>    

    <div class="row">
        <!-- VERIFICAR SE O USUÁRIO CURTIU A PUBLICAÇÃO  -->
        <div class="col-md-12">

            <div class="col-md-6">

                <div id="fb-root"></div>

                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=183517985571747&autoLogAppEvents=1';
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>

                <div class="fb-like" data-href="{{ Request::url() }}" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>

            </div>

            <div class="col-md-6">
                <h3 class="count_view text-right"><b>{{$post_view->views_post->count()}} Visualizações</b></h3>
            </div>

        </div>

    </div>    

    <br>
    <!-- AUTOR -->
    <div class="row">
         <div class="col-md-12 centralizar-verticalmente">
            <div class="col-md-3 col-sm-4 col-xs-4">
                <img class="img-thumbnail box-shadow" src="{{$post_view->user->photo ? $post_view->user->photo->file : 'http://placehold.it/400x400'}}">   
            </div>
            <div class="col-md-10 col-sm-8 col-xs-8">
                 <h4>{{$post_view->user->name}}<br>
            <small><span class="fa fa-clock-o"></span> Posted on {{$post_view->created_at->diffForHumans()}}</small></h4>
            </div> 
        </div>
    </div>
    <br>

    <!-- Blog Comments -->
    <div id="fb-root"></div>

    <script>

        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v3.0&appId=183517985571747&autoLogAppEvents=1';
                fjs.parentNode.insertBefore(js, fjs);
            }
        (document, 'script', 'facebook-jssdk'));

    </script>

    <div class="fb-comments" data-href="{{ Request::url() }}" data-width="100%" data-numposts="5"></div>


    <!-- ajax curtir e descurtir -->
    <script type="text/javascript">

        $.ajaxSetup({

            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }

        });

        $(".curtir").click(function(e){
            e.preventDefault();
            var post_id = $("input[name=post_id]").val();
            $.ajax({
               type:'GET',
               url:'/curtir_post',
               data:{post_id:post_id},
               success:function(data){
                  // alert(data.success);
               }
            });

            $(".form_curtir").hide();

            $(".form_descurtir").show();

            var nro_curtidas = parseInt($("#nro_curtidas").text());
            nro_curtidas_atualizado = nro_curtidas + 1;
            $("#nro_curtidas").text(nro_curtidas_atualizado);

        });

        $(".descurtir").click(function(e){
            e.preventDefault();
            var post_id = $("input[name=post_id]").val();
            $.ajax({
               type:'GET',
               url:'/descurtir_post',
               data:{post_id:post_id},
               success:function(data){
                  // alert(data.success);
               }
            });

            $(".form_descurtir").hide();
            $(".form_curtir").show();
            var nro_curtidas = parseInt($("#nro_curtidas").text());
            nro_curtidas_atualizado = nro_curtidas - 1;
            $("#nro_curtidas").text(nro_curtidas_atualizado);

        });

</script>

@stop