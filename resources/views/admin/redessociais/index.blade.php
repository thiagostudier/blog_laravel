@extends('layouts.admin')

@section('content')

	<h1>Redes Sociais</h1>

	<table class="table">
	    <thead>
	      <tr>
	        <th>Id</th>
	        <th>Nome</th>
	        <th>Link</th>
	        <th>Created</th>
	        <th>Updated</th>
	      </tr>
	    </thead>
	    <tbody>
	    	@if($redesSociais)
	    		@foreach ($redesSociais as $redeSocial)
	    			<tr>
				    	<td>{{$redeSocial->id}}</td>
				    	<td><a href="{{route('redessociais.edit', $redeSocial->id)}}">{{$redeSocial->name}}</a></td>
				    	<td>{{$redeSocial->link}}</td>
				        <td>{{$redeSocial->created_at->diffForHumans()}}</td>
				        <td>{{$redeSocial->updated_at->diffForHumans()}}</td>
				    </tr>	     
	    		@endforeach
	    	@endif
	      
	    </tbody>
	</table>

@stop