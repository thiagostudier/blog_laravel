@extends('layouts.admin')

@section('content')
	<h1>Criar Rede Social</h1>
	
	<div class="col-md-12">
		@include('include.form_error')
	</div>

	{!! Form::open(['method'=>'POST', 'action'=>'RedesSociaisController@store']) !!}
		
		<div class="form-group">
			{!! Form::label('name', 'Nome:') !!}
			{!! Form::text('name', null, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::label('link', 'Link:') !!}
			{!! Form::text('link', null, ['class'=>'form-control']) !!}
		</div>

		<div class="form-group">
			{!! Form::submit('Create User', ['class'=>'btn btn-primary']) !!}
		</div>
 
	{!! Form::close() !!}


@stop
