@extends('layouts.admin')

@section('content')
	
	<h1>Quem Somos</h1>
	
	<table class="table">
	    <thead>
	      <tr>
	        <th>Id</th>
	        <th>Photo</th>
	        <th>Banner</th>
	        <th>Name</th>	       
	      </tr>
	    </thead>
	    <tbody>
	    	@if($quemsomos)
	    		@foreach ($quemsomos as $quemsomos)
	    			<tr>
				    	<td>{{$quemsomos->id}}</td>				    	
				    	<td><img src="{{$quemsomos->logo ? '\codehacking\public\images/'.$quemsomos->logo : 'http://placehold.it/400x400'}}" alt="Imagem" height="50" class=></td>
				    	<td><img src="{{$quemsomos->banner ? '\codehacking\public\images/'.$quemsomos->banner : 'http://placehold.it/400x400'}}" alt="Imagem" height="50" class=></td>
				    	<td><a href="{{ route('quemsomos.edit', $quemsomos->id) }}">{{$quemsomos->name}}</a></td>
				    </tr>	     
	    		@endforeach
	    	@endif
	      
	    </tbody>
	</table>

@stop