@extends('layouts.admin')

@section('content')

	<h1>Edit Quem Somos</h1>

	<div class="col-md-4">
		<img src="{{$quemsomos->logo ? '\codehacking\public\images/'.$quemsomos->logo : 'http://placehold.it/400x400'}}" width="100%" />
	</div>

	<div class="col-md-8">	

		{!! Form::model($quemsomos, ['method'=>'PATCH', 'action'=>['QuemSomosController@update', $quemsomos->id], 'files'=>true]) !!}

			<div class="form-group">
				{!! Form::label('name', 'Name:') !!}
				{!! Form::text('name', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('title') : ""}}
			</div>

			<div class="form-group">
				{!! Form::label('email', 'Email:') !!}
				{!! Form::email('email', null, ['class'=>'form-control']) !!}			
			</div>

			<div class="form-group">
				{!! Form::label('celular', 'Celular:') !!}
				{!! Form::number('celular', null, ['class'=>'form-control']) !!}			
			</div>

			<div class="form-group">
				{!! Form::label('telefone', 'Celular:') !!}
				{!! Form::number('telefone', null, ['class'=>'form-control']) !!}			
			</div>

			<div class="form-group">
				{!! Form::label('logo', 'Logo:') !!}
				{!! Form::file('logo', null,['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('photo_id') : ""}}
			</div>
			<div class="form-group">
				{!! Form::label('banner', 'Banner:') !!}
				{!! Form::file('banner', null,['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('photo_id') : ""}}
			</div>

			<div class="form-group">
				{!! Form::label('lema', 'Lema:') !!}
				{!! Form::text('lema', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('title') : ""}}
			</div>
			<div class="form-group">
				{!! Form::label('historia', 'História:') !!}
				{!! Form::textarea('historia', null, ['class'=>'form-control', 'rows'=>10]) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>	
			<div class="form-group">
				{!! Form::label('objetivo', 'Objetivo:') !!}
				{!! Form::textarea('objetivo', null, ['class'=>'form-control', 'rows'=>10]) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>	
			<div class="form-group">
				{!! Form::label('facebook_page', 'Página do facebook:') !!}
				{!! Form::text('facebook_page', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>	
			<div class="form-group">
				{!! Form::label('twitter_page', 'Página do twitter:') !!}
				{!! Form::text('twitter_page', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>	
			<div class="form-group">
				{!! Form::label('pinterest_page', 'Página do pinterest:') !!}
				{!! Form::text('pinterest_page', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>		
			<div class="form-group">
				{!! Form::submit('Update Quem Somos', ['class'=>'btn btn-primary']) !!}
			</div>	

		{!! Form::close() !!}

		

	</div>

@stop