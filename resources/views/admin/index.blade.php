@extends('layouts.admin')

@section('content')

	<h1>Visualizações</h1>
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<canvas id="myChart"></canvas>
		</div>
	</div>

	<h1>Posts mais Visualizados</h1>

	@if($posts_famosos)
	    @foreach ($posts_famosos as $post)

	    	<div class="panel panel-default">
                <div class="panel-body">
                	<h4><a href="{{url('/post', $post->id)}}"><b>{{$post->title}}</b></a> </h4>
	    			<small><i class="fa fa-user"></i> <a href="{{route('user.show', $post->user->id)}}"> <b>{{$post->user->name}}</b></a></small><br>
                    <small><i class="fa fa-eye"></i> {{$post->views_post->count()}}</small>
	    		</div>
	    	</div>	    	

	    @endforeach
	@endif

	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>

	<script type="text/javascript">

		var hoje = new Date();
		var hoje_1 = new Date();
		var hoje_2 = new Date();
		var hoje_3 = new Date();
		var hoje_4 = new Date();

		var days = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"];

		hoje = hoje.getDate() + " " + days[hoje.getDay()];

		hoje_1.setDate(hoje_1.getDate() - 1);

		hoje_1 = hoje_1.getDate() + " " + days[hoje_1.getDay()];

		hoje_2.setDate(hoje_2.getDate() - 2);

		hoje_2 = hoje_2.getDate() + " " + days[hoje_2.getDay()];

		hoje_3.setDate(hoje_3.getDate() - 3);

		hoje_3 = hoje_3.getDate() + " " + days[hoje_3.getDay()];

		hoje_4.setDate(hoje_4.getDate() - 4);

		hoje_4 = hoje_4.getDate() + " " + days[hoje_4.getDay()];

		new Chart(document.getElementById("myChart"), {
			  type: 'line',
			  data: {
			    labels: [hoje_4,hoje_3,hoje_2,hoje_1,hoje],
			    datasets: [{ 
			        data: [{{$views_hoje_4}},{{$views_hoje_3}},{{$views_hoje_2}},{{$views_hoje_1}},{{$views_hoje}}],
			        label: "Número de Visualizações",
			        borderColor: "#3e95cd",
			        fill: false
			      }
			    ]
			  },
			  options: {
			    title: {
			      display: true,
			      // text: 'World population per region (in millions)'
			    }
			  }
		});
	</script>
@stop