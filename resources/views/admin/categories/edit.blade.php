@extends('layouts.admin')

@section('content')

	<h1>Edit Categories</h1>

	<div class="col-md-6">
		{!! Form::model($category, ['method'=>'PATCH', 'action'=>['AdminCategoriesController@update', $category->id]]) !!}
			<div class="form-group">
				{!! Form::label('name', 'Category:') !!}
				{!! Form::text('name', null, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::submit('Update Category', ['class'=>'btn btn-primary']) !!}
			</div>
		{!! Form::close() !!}

		{!! Form::model($category, ['method'=>'DELETE', 'action'=>['AdminCategoriesController@destroy', $category->id]]) !!}
			{!! Form::submit('Delete Category', ['class'=>'btn btn-danger']) !!}
		{!! Form::close() !!}

	</div>

@stop