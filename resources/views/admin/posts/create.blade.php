@extends('layouts.admin')

@section('content')

	<h1>Create Post</h1>

	<!-- <div class="col-md-12">
		@include('include.form_error')
	</div> -->

	{!! Form::open(['method'=>'POST', 'action'=>'AdminPostsController@store', 'files'=>true]) !!}

		<div class="form-group">
			{!! Form::label('title', 'Title:') !!}
			{!! Form::text('title', null, ['class'=>'form-control']) !!}
			{{isset($errors) ? $errors->first('title') : ""}}
			
		</div>
		<div class="form-group">
			{!! Form::label('category_id', 'Category:') !!}
			{!! Form::select('category_id', [''=>'Choose Options'] + $categories, null, ['class'=>'form-control']) !!}
			{{isset($errors) ? $errors->first('category_id') : ""}}
		</div>
		<div class="form-group">
			{!! Form::label('photo_id', 'Category:') !!}
			{!! Form::file('photo_id', null,['class'=>'form-control']) !!}
			{{isset($errors) ? $errors->first('photo_id') : ""}}
		</div>
		<div class="form-group">
			{!! Form::label('body', 'Body:') !!}
			{!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>10, 'id'=>'edit']) !!}
			{{isset($errors) ? $errors->first('body') : ""}}
		</div>	
		<div class="form-group">
			{!! Form::label('is_active', 'Status:') !!}
			{!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), 0, ['class'=>'form-control']) !!}	
		</div>		
		<div class="form-group">
			{!! Form::label('destaque_aside', 'Destaque Lateral:') !!}
			{!! Form::select('destaque_aside', array(1 => 'Sim', 0=> 'Não'), null, ['class'=>'form-control']) !!}
		</div>
		<div class="form-group">
			{!! Form::label('destaque_home', 'Destaque Inicial:') !!}
			@if($posts_header >= 3)
				<p>O número máximo de Postagens no Destaque Inicial são de três</p>
			@else				
				{!! Form::select('destaque_home', array(1 => 'Sim', 0=> 'Não'), null, ['class'=>'form-control']) !!}
			@endif
		</div>
		<div class="form-group">
			{!! Form::submit('Create Post', ['class'=>'btn btn-primary']) !!}
		</div>	

	{!! Form::close() !!}
	

@stop