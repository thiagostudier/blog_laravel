@extends('layouts.admin')

@section('content')

	<h1>Edit Posts</h1>

	<div class="col-md-4">
		<img src="{{$post->photo ? $post->photo->file : 'http://placehold.it/400x400'}}" width="100%" />
	</div>

	<div class="col-md-8">	

		{!! Form::model($post, ['method'=>'PATCH', 'action'=>['AdminPostsController@update', $post->id], 'files'=>true]) !!}

			<div class="form-group">
				{!! Form::label('title', 'Title:') !!}
				{!! Form::text('title', null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('title') : ""}}
				
			</div>
			<div class="form-group">
				{!! Form::label('category_id', 'Category:') !!}
				{!! Form::select('category_id', [''=>'Choose Options'] + $categories, null, ['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('category_id') : ""}}
			</div>
			<div class="form-group">
				{!! Form::label('photo_id', 'Category:') !!}
				{!! Form::file('photo_id', null,['class'=>'form-control']) !!}
				{{isset($errors) ? $errors->first('photo_id') : ""}}
			</div>
			<div class="form-group">
				{!! Form::label('body', 'Body:') !!}
				{!! Form::textarea('body', null, ['class'=>'form-control', 'rows'=>10, 'id'=>'edit']) !!}
				{{isset($errors) ? $errors->first('body') : ""}}
			</div>	
			<div class="form-group">
				{!! Form::label('is_active', 'Status:') !!}
				{!! Form::select('is_active', array(1 => 'Active', 0=> 'Not Active'), null, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('destaque_aside', 'Destaque Lateral:') !!}
				{!! Form::select('destaque_aside', array(1 => 'Sim', 0=> 'Não'), null, ['class'=>'form-control']) !!}
			</div>
			<div class="form-group">
				{!! Form::label('destaque_home', 'Destaque Inicial:') !!}
				{!! Form::select('destaque_home', array(1 => 'Sim', 0=> 'Não'), null, ['class'=>'form-control']) !!}
			</div>		
			<div class="form-group">
				{!! Form::submit('Update Post', ['class'=>'btn btn-primary']) !!}
			</div>	

		{!! Form::close() !!}

		{!! Form::model($post, ['method'=>'DELETE', 'action'=>['AdminPostsController@destroy', $post->id]]) !!}
			<div class="form-group">
				{!! Form::submit('Delete Post', ['class'=>'btn btn-danger']) !!}
			</div>
		{!! Form::close() !!}

	</div>

@stop