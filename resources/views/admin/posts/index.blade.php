@extends('layouts.admin')

@section('content')

	<h1>Destaque Inicial</h1>

	<table class="table">
	    <thead>
	      <tr>
	        <th>Status</th>
	        <th>Editor</th>	       
	        <th>Category</th>
	        <th>Title</th>
	        <th>View Post</th>
	        <th>Comments</th>
	        <th>Created at</th>
	        <th>Updated at</th>
	      </tr>
	    </thead>
	    <tbody>
	    	@if(isset($post_header))
	    		@foreach ($post_header as $pd)
	    			<tr>
				    	<td>{{$pd->is_active ? 'Ativo' : 'Desativo'}}</td>				    	
				    	<td>{{$pd->user->name}}</td>
				    	<td>{{$pd->category->name}}</td>
				    	<td><a href="{{ route('posts.edit', $pd->id) }}">{{$pd->title}}</a></td>
				    	<td><a href="{{route('home.post', $pd->id)}}">Post</a></td>
				    	<td><a href="{{route('comments.show', $pd->id)}}">Comments</a></td>
				    	<td>{{$pd->created_at->diffForHumans()}}</td>
				    	<td>{{$pd->updated_at->diffForHumans()}}</td>
				    </tr>	     
	    		@endforeach
	    	@else 
	    		<tr><td colspan="8" align="center">Nenhum destaque selecionado</td></tr>
	    	@endif
	      
	    </tbody>
	</table>

	<h1>Destaque Lateral</h1>

	<table class="table">
	    <thead>
	      <tr>
	        <th>Status</th>
	        <th>Editor</th>	       
	        <th>Category</th>
	        <th>Title</th>
	        <th>View Post</th>
	        <th>Comments</th>
	        <th>Created at</th>
	        <th>Updated at</th>
	      </tr>
	    </thead>
	    <tbody>
	    	@if(isset($posts_aside))
	    		@foreach ($posts_aside as $pd)
	    			<tr>
				    	<td>{{$pd->is_active ? 'Ativo' : 'Desativo'}}</td>				    	
				    	<td>{{$pd->user->name}}</td>
				    	<td>{{$pd->category->name}}</td>
				    	<td><a href="{{ route('posts.edit', $pd->id) }}">{{$pd->title}}</a></td>
				    	<td><a href="{{route('home.post', $pd->id)}}">Post</a></td>
				    	<td><a href="{{route('comments.show', $pd->id)}}">Comments</a></td>
				    	<td>{{$pd->created_at->diffForHumans()}}</td>
				    	<td>{{$pd->updated_at->diffForHumans()}}</td>
				    </tr>	     
	    		@endforeach
	    	@else 
	    		<tr><td colspan="8" align="center">Nenhum destaque selecionado</td></tr>
	    	@endif
	      
	    </tbody>
	</table>

	<h1>Posts</h1>
	
	<table class="table">
	    <thead>
	      <tr>
	        <th>Status</th>
	        <th>Editor</th>	       
	        <th>Category</th>
	        <th>Title</th>
	        <th>View Post</th>
	        <th>Comments</th>
	        <th>Created at</th>
	        <th>Updated at</th>
	      </tr>
	    </thead>
	    <tbody>
	    	@if($posts)
	    		@foreach ($posts as $post)
	    			<tr>
				    	<td>{{$post->is_active ? 'Ativo' : 'Desativo'}}</td>				    	
				    	<td>{{$post->user->name}}</td>
				    	<td>{{$post->category->name}}</td>
				    	<td><a href="{{ route('posts.edit', $post->id) }}">{{$post->title}}</a></td>
				    	<td><a href="{{route('home.post', $post->id)}}">Post</a></td>
				    	<td><a href="{{route('comments.show', $post->id)}}">Comments</a></td>
				    	<td>{{$post->created_at->diffForHumans()}}</td>
				    	<td>{{$post->updated_at->diffForHumans()}}</td>
				    </tr>	     
	    		@endforeach
	    	@endif
	      
	    </tbody>
	</table>

	<div class="row">
		<div class="col-sm-4 col-sm-offset-5">
			{{$posts->render()}}
		</div>
	</div>

@stop