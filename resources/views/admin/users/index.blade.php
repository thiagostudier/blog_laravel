@extends('layouts.admin')

@section('content')

	@if((session('success')))
		<p class="alert alert-success">{{session('success')}}</p>
	@endif
	

	<h1>Users</h1>

	<div class="col-md-12">

        @if (count($users) > 0)
            <section class="articles">
                @include('admin.users.load')
            </section>
        @endif

    </div>
    
	<script src="{{asset('js/libs.js')}}"></script>

    <script type="text/javascript">

        $(function() {
            $('body').on('click', '.pagination a', function(e) {
                e.preventDefault();

                $('#load a').css('color', '#dfecf6');
                $('#load').append('Carregando...');

                var url = $(this).attr('href');
                getArticles(url);
                window.history.pushState("", "", url);
            });

            function getArticles(url) {
                $.ajax({
                    url : url
                }).done(function (data) {
                    $('.articles').html(data);
                }).fail(function () {
                    alert('Articles could not be loaded.');
                });
            }
        });
    </script>

@stop