<div id="load" style="position: relative;">
	<table class="table">
	    <thead>
	      <tr>
	        <th>Id</th>
	        <th>Photo</th>
	        <th>Name</th>
	        <th>Email</th>
	        <th>Role</th>
	        <th>Active</th>
	        <th>Created</th>
	        <th>Updated</th>
	      </tr>
	    </thead>
	    <tbody>
	    	@if($users)
	    		@foreach ($users as $user)
	    			<tr>
				    	<td>{{$user->id}}</td>
				    	<td><img style="height:50px;" src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" class="img-responsive img-rounded"></img></td>
				        <td><a href="{{route('users.edit', $user->id)}}">{{$user->name}}</a></td>
				        <td>{{$user->email}}</td>
				        <td>{{isset($user->role->name) ? $user->role->name : 'User has no role' }}</td>
				        <td>{{$user->is_active == 1 ? 'Active' : 'No active'}}</td>
				        <td>{{$user->created_at->diffForHumans()}}</td>
				        <td>{{$user->updated_at->diffForHumans()}}</td>
				    </tr>	     
	    		@endforeach
	    	@endif
	      
	    </tbody>
	</table>
</div>
{{ $users->links() }}