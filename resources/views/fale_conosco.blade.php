@extends('layouts.blog-home')

@section('content')
	<div class="col-md-8">
		<form method="post" class="col-md-12" action="enviar">
			@if(Session::has('message_success'))
				<p class="alert alert-success">{{session('message_success')}}</p>
			@endif
			<h1>Fale Conosco</h1>
			{{ csrf_field() }}
			<div class="form-group">			
				{!! Form::text('nome', null, ['class'=>'form-control', 'placeholder'=>'Nome']) !!}
			</div>

			<div class="form-group">
				{!! Form::email('email', null, ['class'=>'form-control', 'placeholder'=>'Email']) !!}
			</div>

			<div class="form-group">
				{!! Form::textarea('mensagem', null, ['class'=>'form-control', 'placeholder'=>'Mensagem']) !!}
			</div>

			<div class="form-group">
				{!! Form::submit('Enviar', ['class'=>'btn btn-primary']) !!}
			</div>

		</form>
	</div>

	<div class="col-md-4">
        @include('include.user_admin')
    </div>

@stop