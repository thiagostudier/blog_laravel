@extends('layouts.blog-home')

@section('content')

	<h1>Editar Usuário</h1>

	@if((session('success')))
		<p class="alert alert-success">{{session('success')}}</p>
	@endif

	@if((session('erro')))
		<p class="alert alert-danger">{{session('erro')}}</p>
	@endif
	

	<div class="col-sm-3">

		<img src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" class="img-responsive img-rounded"></img>
		
	</div>

	<div class="col-sm-9">
		{!! Form::model($user, ['method'=>'PATCH', 'action'=>['UserController@update', $user->id], 'files'=>true]) !!}
			
			<div class="form-group">
				{!! Form::label('name', 'Title:') !!}
				{!! Form::text('name', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('email', 'Title:') !!}
				{!! Form::text('email', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('about', 'Sobre:') !!}
				{!! Form::textarea('about', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('facebook', 'www.facebook.com/') !!}
				{!! Form::text('facebook', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('twitter', 'www.twitter.com/') !!}
				{!! Form::text('twitter', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('name', 'www.pinterest.com/') !!}
				{!! Form::text('pinterest', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('name', 'www.instagram.com/') !!}
				{!! Form::text('instagram', null, ['class'=>'form-control']) !!}
			</div>

			<div class="form-group">
				{!! Form::label('photo_id', 'File:') !!}
				{!! Form::file('photo_id', null, ['class'=>'form-control']) !!}			
			</div>

			<div class="form-group">
				{!! Form::submit('Update User', ['class'=>'btn btn-primary']) !!}
			</div>
	 
		{!! Form::close() !!}

	</div>

	
	<div class="col-md-12">
		@include('include.form_error')
	</div>
	


@stop
