@extends('layouts.blog-post')

@section('content')

	<div class="col-md-4">
		<h2 align="center">{{$user->name}}</h2>			
		<div class="col-md-12">
			<img src="{{$user->photo ? $user->photo->file : 'http://placehold.it/400x400'}}" class="img-responsive img-rounded"></img>	
			<br>
			<a style="text-align: center;" href="{{ url('user/edit') }}" class="btn btn-primary">Editar</a>
		</div>
	</div>

	<div class="col-md-12">
		<br>
		<h3>Favoritos:</h3>
		<br>
    	@if(isset($posts))
    		@foreach ($posts as $pd)

    		<div class="panel panel-default">
			  	<div class="panel-heading">
			    	<h3 class="panel-title"><a href="{{route('home.post', $pd->id)}}">{{$pd->title}} <small>{{$pd->created_at->diffForHumans()}}</small></a></h3>
			  	</div>
			  	<div class="panel-body">
			    	<p>Autor: {{$pd->category->name}}</p>
			    	<p>{{$pd->user->name}}</p>
			    	<p><?=substr($pd->body, 0, 200)."...";?></p>
			  	</div>			  	
			</div>
    				     
    		@endforeach

    		<div class="row">
				<div class="col-sm-4 col-sm-offset-5">
					{{$posts->render()}}
				</div>
			</div>		
    	@else 
    		<p>Nenhuma postagem encontrada</p>
    	@endif
		      
	</div>

	<div class="col-md-12">
		@include('include.form_error')
	</div>
	


@stop
