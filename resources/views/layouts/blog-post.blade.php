@include('include.head')

<body>

    <!-- Navigation -->

    @include('include.menu')


    <!-- Page Content -->
    <div class="container margin-top-big">

        <div class="row">
            @if(Auth::user() and Auth::user()->is_active == 0) 
                <div class="col-md-12">
                  <div class="alert alert-warning" role="alert">
                    Acesse seu email para validar sua conta
                  </div>
                </div>              
            @endif
            <!-- Blog Post Content Column -->
            <div class="col-md-8 postagem">
                @yield('content')
            </div>

            <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
              
                @include('include.aside')
                
                @include('include.user_admin')
                
            </div>

        </div>
        <!-- /.row -->        

    </div>
    <!-- /.container -->
    <!-- Footer -->
    <footer>
        @include('include.footer')
    </footer>

    <script src="{{asset('js/libs.js')}}"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $( function() {
        
        $("#searchItem").autocomplete({
          source: 'http://dry-hollows-38671.herokuapp.com/search_ajax_post'
        });

        $(".dropdown-toggle").dropdown();

      });
    </script> 

</body>

</html>
