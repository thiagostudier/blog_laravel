@include('include.head')

<body>

    <!-- Navigation -->

    @include('include.menu')

    @yield('header')

    <div class="container margin-top-big">

      @if(Auth::user() and Auth::user()->is_active == 0) 
        <div class="col-md-12">
          <div class="alert alert-warning" role="alert">
            Acesse seu email para validar sua conta
          </div>
        </div>              
      @endif

      @if((session('success_validar_email')))
         <div class="col-md-12">
          <div class="alert alert-success" role="alert">
            {{session('success_validar_email')}}
          </div>
        </div>
      @endif  
      
        <div class="row">
          @yield('content')
        </div>
    </div>

    <footer>
        @include('include.footer')
    </footer>
     <!-- jQuery -->
    <script src="{{asset('js/libs.js')}}"></script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>

    <script>
      $( function() {

         $(window).on('scroll', function(e){
              //aparece segundo menu
              if($(this).scrollTop() == 0){
                  
              }else{
                  
              }

          });            

        $('.slide').slick({
          autoplay: true,
          dots: true,
          infinite: true,
          speed: 500,
          slidesToShow: 1,
          adaptiveHeight: true
        });

        $(".dropdown-toggle").dropdown();
        
      } );      

    </script>
</body>

</html>
