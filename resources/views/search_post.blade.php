@extends('layouts.blog-post')

@section('content')

	<h1>Resultados</h1>
	@if(isset($pesquisa))
		<p><small>Você pesquisou por "{{$pesquisa}}"</small></p>
	@endif
	<section class="col-md-12">

		<div class="row">				
			<form class="pesquisar" action="search_post" method="get">
				<div class="form-group col-md-12">
					<div class="col-md-8 col-sm-10">
						<input type="text" class="form-control" name="searchItem" id="searchItem2" placeholder="Procure uma postagem">
					</div>
					<div class="col-md-4 col-sm-2">
						<input type="submit" class="btn btn-primary" value="Pesquisar">							
					</div>
				</div>
			</form>
		</div>

		<div class="row">

			<?php $count = 0; ?>

			@foreach($posts_search as $post)
				<?php $count = $count + 1; ?>
				<div class="col-md-6">
					<a href="{{ route('home.post', $post->id) }}" class="col-md-12 item-photo-pequena item fundo-preto" >
						<img class="banner-post img-responsive" src="{{$post->photo->file}}">
						<div class="desc-item-post col-md-12">
							<h6>{{$post->created_at->diffForHumans()}}</h6>
							<h4>{{$post->title}}</h4>
						</div>	
					</a>										
				</div>	

			@endforeach

			<?php 

				if($count == 0){
					echo "<div class='col-md-6'>";
						echo "<p>Nenhum resultado encontrado!</p>";
					echo "</div>";
				}

			?>

		</div>	
	
		
	</section>
		

@endsection