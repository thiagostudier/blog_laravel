@extends('layouts.blog-home')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>
                <div class="panel-body">

                    <form class="form-horizontal" method="post" action="{{url('/login/social')}}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="email" class="col-md-4 control-label">Redes Sociais:</label>

                            <div class="col-md-6">
                                  
                                <button type="submit" class="btn btn-social btn-facebook" style="background: #4267b2;color: #fff;" value="facebook" name="social_type"><i class="fa fa-facebook"></i>  Entrar com facebook</button>
                            </div>
                        </div> 

                    </form> 

                    <div class="col-md-8 col-md-offset-2">
                        <hr>
                    </div>
                    
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}">

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="about" class="col-md-4 control-label">Sobre</label>

                            <div class="col-md-6">
                                <textarea id="about" name="about" class="form-control" value="{{ old('about') }}"></textarea>

                                @if ($errors->has('about'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="facebook" class="col-md-4 control-label">www.facebook.com/</label>

                            <div class="col-md-6">
                                <input id="facebook" type="text" name="facebook" placeholder="Seu Usuário" class="form-control" value="{{ old('facebook') }}"/>

                                @if ($errors->has('facebook'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('facebook') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="twitter" class="col-md-4 control-label">www.twitter.com/</label>

                            <div class="col-md-6">
                                <input id="twitter" type="text" name="twitter" placeholder="Seu Usuário" class="form-control" value="{{ old('twitter') }}"/>

                                @if ($errors->has('twitter'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('twitter') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="instagram" class="col-md-4 control-label">www.instagram.com/</label>

                            <div class="col-md-6">
                                <input id="instagram" type="text" name="instagram" placeholder="Seu Usuário" class="form-control" value="{{ old('instagram') }}"/>

                                @if ($errors->has('instagram'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('instagram') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="pinterest" class="col-md-4 control-label">www.pinterest.com/</label>

                            <div class="col-md-6">
                                <input id="pinterest" type="text" name="pinterest" placeholder="Seu Usuário" class="form-control" value="{{ old('pinterest') }}"/>

                                @if ($errors->has('pinterest'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('pinterest') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Senha</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-4 control-label">Confirmar Senha</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation">

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
