@extends('layouts.blog-home')

@section('content')
	<section class="col-md-12" id="home_post_inicio">

		<div class="slide row">

			<?php $count = 0; ?>

			@foreach($posts_header as $post_header)

				<?php $count = $count + 1; ?>

				@if( ($count % 3) == 0 )
					<div class="col-md-8 hidden-xs hidden-sm">
						<a href="{{ route('home.post', $post_header->id) }}" class="col-md-12 item-photo-grande item fundo-preto" >
				@elseif( ($count % 3) == 1 )
				
					<div class="col-md-12">			
					<div class="col-md-4">
						<div class="row">
							<div class="col-md-12">
								<a href="{{ route('home.post', $post_header->id) }}" class="col-md-12 item-photo-media item fundo-preto" >
				@elseif( ($count % 3) == 2 )
					<div class="row">
						<div class="col-md-12">
							<a href="{{ route('home.post', $post_header->id) }}" class="col-md-12 item-photo-media item fundo-preto" >
				@endif
							<img class="banner-post img-responsive" src="{{$post_header->photo->file}}">
							<div class="desc-item-post col-md-12">
								<h3>{{$post_header->category->name}} <small>{{$post_header->created_at->diffForHumans()}} (<b><?=$post_header->created_at->formatLocalized('%d/%b/%y')?></b>)</small></h3>								
								<h4>{{$post_header->title}}</h4>
							</div>	
						</a>
				@if( ($count % 3) == 0 )
					</div>
					</div>	
				@elseif( ($count % 3) == 1 )
					</div>					
					</div>					
				@elseif( ($count % 3) == 2 )
					</div>
					</div>
					</div>
				@endif
			@endforeach
			
		</div>

	</section>

	<section id="pages" class="col-md-12">
		<div class="col-md-8">
			<h1><b>Recentes</b></h1>		
		</div>
		<article class="col-md-8">			
			<div class="row">
				@if (count($posts) > 0)

                    <section class="articles">

						<!-- @include('include.ajax.posts_home') -->
						
						@foreach($posts as $post)

							<div class="col-md-12 item-article centralizar-verticalmente">
								<a href="{{ route('home.post', $post->id) }}" class="col-md-6 item-photo-pequena item fundo-preto" >
									<img class="banner-post-home img-responsive" src="{{$post->photo->file}}">
								</a>
								<div class="col-md-6">
									<h5 class="media-heading">{{$post->category->name}}
						                <small>{{$post->created_at->diffForHumans()}} (<b><?=$post->created_at->formatLocalized('%d/%b/%y')?></b>)</small>
						            </h5>
									<h2><b>{{$post->title}}</b></h2>
									<h6><b><?=str_limit($post->body, 100)?></b></h6>
									<h4><small>Autor: {{$post->user->name}}</small></h4>

									<h4><small><a href="{{route('home.post', $post->id)}}">Saiba mais</a></small></h4>
								</div>
							</div>
										
						@endforeach
						<div class="col-md-12">
							<button class="btn btn-primary" id="carregar_noticias"><span class="fa fa-search"></span> Mais Notícias</button>
						</div>

                    </section>
                @endif
			</div>
		</article>
		<aside class="col-md-4">
			@include('include.aside')
			
			@include('include.user_admin')
		</aside>		
	</section>

	<script src="{{asset('js/libs.js')}}"></script>

    <script type="text/javascript">

        $(function() {

        	$.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	            }
	        });

        	var count = 0;

            $('body').on('click', '#carregar_noticias', function() {

                count = count + 10;

            	console.log(count);

                // $('.articles').html("<h2 id='load_posts'><i class='fa fa-refresh fa-spin fa-fw'></i> Carregando</h2>");

                $.ajax({
                cache: false,
                url:"/more_posts", 
                type: 'get',
                dataType: 'json',
                // data: {count:count},
                
                success: function () {

                    alert('foi');

                    // $("#carregando").remove();

                },
                error: function(){

                    // $("#carregando").remove();

                    alert('erro');

                }

            });

            });
        });
    </script>

@endsection
