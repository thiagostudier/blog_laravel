<?php
    use App\QuemSomos;
    $empresa = QuemSomos::all();
    $nome_title = QuemSomos::limit(1)->value('name');
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    @foreach($empresa as $logo)
        @if(isset($logo->logo) and $logo->logo != '')
            <link rel="icon" href="{{$logo->logo}}">
        @endif
    @endforeach    

    @if(isset($post_view->id))
        <meta name="author" content="{{$nome_title}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta property="og:url" content="{{ Request::url() }}" />
        <meta property="og:type" content="{{$nome_title}}" />
        <meta property="og:title" content="{{$post_view->name}}" />
        <meta property="og:description" content="{{strip_tags(str_limit($post_view->body, 200))}}" />
        <meta property="og:image" content="{{$post_view->photo->file}}" />

        <title>| {{$post_view->title}}</title> 
    @elseif(isset($category))

        <!-- <meta property="og:image" content="http://thiagostudier.xyz/{{route('category', $category->id)}}"> -->
        <meta name="description" content="{{$category->name}}">
        <meta name="keywords" content="{{$category->name}},{{$nome_title}}">
        <meta name="author" content="{{$nome_title}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">  

        <title>| {{$category->name}}</title>
    @elseif(isset($nome_title))
        <meta name="description" content="{{$nome_title}}">
         <meta name="keywords" content="{{$nome_title}}">
        <meta name="author" content="{{$nome_title}}">
        <title>| {{$nome_title}}</title>
    @endif

    </title>

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/app.css')}}" rel="stylesheet">    
    <link href="{{asset('css/libs.css')}}" rel="stylesheet">
    <link href="{{asset('font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">

    <!-- BIBLIOTECA PARA O 'SEARCH' -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <!-- FONT AWESOME -->
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css"> -->

    <!-- SLIDE -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css">
    <!-- BOTÃO COMPARTILHAR -->
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5a9d7e44e3a41ab1"></script> 

</head>