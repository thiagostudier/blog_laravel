<?php
	use App\QuemSomos;
	$empresa = QuemSomos::limit(1)->get();
?>
	@foreach($empresa as $empresaa)	
	<header class="centralizar-verticalmente">
		<img class="img-responsive img-header-back" src="{{$empresaa->banner ? $empresaa->banner : 'http://placehold.it/400x400'}}">
		<div class="container">
			<div class="col-md-4 col-md-offset-4 intro-header">
				<h1 class="titulo-header"><b>{{$empresaa->name}}</b><br>
					<small>"{{$empresaa->lema}}"</small>				
				</h1>

			</div>
		</div>

	</header>
	@endforeach
	
