<?php
	use App\Category;
	use App\QuemSomos;
	use App\RedesSociais;
	$categories = Category::all();
	$empresa = QuemSomos::all();
	$redesSociais = RedesSociais::all();
?>
<div class="container">
	<br>
	<br>
	<div class="row">
		<div class="col-md-12 centralizar-verticalmente">
			<div class="col-md-4">
				<!-- facebook button -->
				@foreach($empresa as $facebook_page)
					@if(isset($facebook_page->facebook_page) and $facebook_page->facebook_page != '')
					    <div id="fb-root"></div>
					    <script>(function(d, s, id) {
					      var js, fjs = d.getElementsByTagName(s)[0];
					      if (d.getElementById(id)) return;
					      js = d.createElement(s); js.id = id;
					      js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.12&appId=482432195296644&autoLogAppEvents=1';
					      fjs.parentNode.insertBefore(js, fjs);
					    }(document, 'script', 'facebook-jssdk'));</script>

					   	<div class="fb-like" style="color:white !important;" data-href="{{$facebook_page->facebook_page}}" data-width="50px" data-layout="standard" data-action="like" data-size="small" data-show-faces="true" data-share="true"></div>
					   	
					@endif
				@endforeach
				<br>
			   	<!-- seguiter twitter -->
			   	@foreach($empresa as $twitter_page)
					@if(isset($twitter_page->twitter_page) and $twitter_page->twitter_page != '')
						<script type="text/javascript" src="http://platform.twitter.com/widgets.js">
						</script>
						<br>
						<br>		
						<a class="twitter-follow-button"
						  href="{{$twitter_page->twitter_page}}">
						Seguir @tsstudier</a>						
					@endif
				@endforeach
			   	<br>
				<br>
				<!-- pinterest -->
				@foreach($empresa as $pinterest_page)
					@if(isset($pinterest_page->pinterest_page) and $pinterest_page->pinterest_page != '')
						<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
						<a data-pin-do="buttonFollow" href="https://br.pinterest.com/thiagostudier/">Seguir</a>
					@endif
				@endforeach		
			</div>

			<div class="col-md-2 col-sm-6 col-sm-offset-3 col-xs-8 col-xs-offset-2">
				@foreach($empresa as $logo)
					<br>
					<img class="img-responsive" src="{{$logo->logo ? $logo->logo : 'http://placehold.it/400x400'}}" alt="Logo {{$logo->name}}">
					<br>
				@endforeach
			</div>
			<div class="col-md-6" style="color:white;text-align:center;">
				@foreach($empresa as $intro)
					<h2>{{$intro->name}}</h2>
					<h3>{{$intro->lema}}</h3>
				@endforeach
			</div>
		</div>
	</div>
	<br>
	<br>
	<div class="row">
		<div class="col-md-12">
			<ul class="lista-footer col-md-3">
				<li><h4 class="title-lista-footer">Categorias</h4></li>
				@foreach($categories as $category)
					<li>
				    	<h4><b><a href="{{ route('category', $category->id) }}">{{$category->name}}</a></b></h4> 
					</li>
				@endforeach
			</ul>
			<ul class="lista-footer col-md-3">
				<li><h4 class="title-lista-footer">Sobre</h4></li>
				<li><h4><b><a href="{{route('quemsomos')}}">Quem somos</a></b></h4></li>
				<li><h4><b><a href="{{url('fale_conosco')}}">Fale conosco</a></b></h4></li>
			</ul>
			<ul class="lista-footer col-md-3">
				<li><h4 class="title-lista-footer">Redes Sociais</h4></li>
				@foreach($redesSociais as $redeSocial)
					
					<li><h4><b><a href="{{$redeSocial->link}}" target="_blank">{{$redeSocial->name}}</a></b></h4></li>
				@endforeach
			</ul>
			<ul class="lista-footer col-md-3">
				<li><h4 class="title-lista-footer">Contato</h4></li>
				@foreach($empresa as $empresaa)
					<li><h4><b><a>{{$empresaa->email}}</a></b></h4></li>
					<li><h4><b><a>Telefone: {{$empresaa->telefone}}</a></b></h4></li>
					<li><h4><b><a>Celular: {{$empresaa->celular}}</a></b></h4></li>
				@endforeach
			</ul>			
		</div>		
	</div>
	<div class="row">
		<div class="col-md-12">
			<h4 align="right"><small>Desenvolvido por Thiago Studier, 2018</small></h4>
		</div>
	</div>
</div>

	
