<?php
	use App\User;

	$users_admin = User::where('role_id',1)->orderBy('updated_at','desc')->get();
?>

<h3 class="text-center">Administrador(es)</h3>

@foreach($users_admin as $admin)
	<div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-8 col-xs-offset-2">
        <img class="img-thumbnail" src="{{$admin->photo ? $admin->photo->file : 'http://placehold.it/400x400'}}">
    </div>
    <div class="col-md-12 text-center">
    	<h4><b>{{$admin->name}}</b></h4>
    </div>
    <div class="col-md-12">
    	<!-- <p>{{$admin->about}}</p> -->
        @if(isset($admin->twitter) and $admin->twitter != '')
            <script type="text/javascript" src="http://platform.twitter.com/widgets.js">
            </script> 
            <p><a class="twitter-follow-button"
              href="https://twitter.com/{{$admin->twitter}}">
            Seguir</a></p>                       
        @endif                
                    
        @if(isset($admin->pinterest) and $admin->pinterest != '')
            <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
            <p><a data-pin-do="buttonFollow" href="https://br.pinterest.com/{{$admin->pinterest}}">Seguir</a></p>
        @endif
    </div>

@endforeach