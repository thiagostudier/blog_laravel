<?php
    use App\QuemSomos;
    use App\Category;
    $categories = Category::all();
    $empresa = QuemSomos::all();
    $nome_title = QuemSomos::limit(1)->value('name');
?>
<nav class="navbar navbar-inverse navbar-fixed-top background-transparent" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand centralizar-verticalmente" href="{{ url('/') }}">
                @foreach($empresa as $menu)
                    @if(isset($menu->logo) and $menu->logo != '')
                        <img style="float:left;" class="d-inline-block align-top" width="30" height="30" src="{{$menu->logo}}">  
                        <span style="margin-left:20px;">{{$menu->name}}</span>
                     @endif
                @endforeach 
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
           
            <ul class="nav navbar-nav navbar-right">
                <!-- Authentication Links -->
                @foreach($categories as $category)
                    <li>
                        <a href="{{ route('category', $category->id) }}">{{$category->name}}</a>
                    </li>

                @endforeach 
                @if (Auth::guest())
                    <li><a href="{{ url('/login') }}">Login</a></li>
                    <li><a href="{{ url('/register') }}">Register</a></li>
                @else
                    <li><a><img class="img-responsive img-circle" width="30px" height="20px" src="{{Auth::user()->photo ? Auth::user()->photo->file : 'http://placehold.it/400x400'}}"></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            {{ Auth::user()->name }} 

                            <i class="fa fa-caret-down" aria-hidden="true"></i>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            @if(Auth::user()->is_active != 0)
                                @if(Auth::user()->role_id != 3) <!--  se o usuário foi admin ou editor, pode entrar na tela de administrador -->
                                    <li><a href="{{ url('/admin') }}"><i class="fa fa-code"></i> Admin</a></li>    
                                @endif                        
                                <li><a href="{{ url('/user') }}"><i class="fa  fa-pencil-square-o"></i> Dashboard</a></li>
                            @endif  
                            <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i> Logout</a></li>
                        </ul>
                    </li>
                @endif                
            </ul>

             <form class="pesquisar navbar-form navbar-right" action="\search_post" method="get">
                <div class="form-group">
                    <input type="search" class="form-control mr-sm-2" aria-label="Search" name="searchItem" id="searchItem2" placeholder="Procure uma postagem">
                </div>
                <button type="submit" class="btn btn-outline-primary my-2 my-sm-0" value="Pesquisar">
                    <span class="fa fa-search" aria-hidden="true"></span>
                </button>
                <!-- <input type="submit" class="btn btn-outline-primary my-2 my-sm-0" value="Pesquisar">          -->
            </form>
            <script src="{{asset('js/libs.js')}}"></script>

            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
            <script type="text/javascript">
                
                $("#searchItem, #searchItem2").autocomplete({
                  source: "http://dry-hollows-38671.herokuapp.com/search_ajax_post"
                });
            </script>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>
