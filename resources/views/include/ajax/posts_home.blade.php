@foreach($posts as $post)

	<div class="col-md-12 item-article centralizar-verticalmente">
		<a href="{{ route('home.post', $post->id) }}" class="col-md-6 item-photo-pequena item fundo-preto" >
			<img class="banner-post-home img-responsive" src="{{$post->photo->file}}">
		</a>
		<div class="col-md-6">
			<h5 class="media-heading">{{$post->category->name}}
                <small>{{$post->created_at->diffForHumans()}} (<b><?=$post->created_at->formatLocalized('%d/%b/%y')?></b>)</small>
            </h5>
			<h2><b>{{$post->title}}</b></h2>
			<h6><b><?=str_limit($post->body, 100)?></b></h6>
			<h4><small>Autor: {{$post->user->name}}</small></h4>

			<h4><small><a href="{{route('home.post', $post->id)}}">Saiba mais</a></small></h4>
		</div>
	</div>
				
@endforeach
