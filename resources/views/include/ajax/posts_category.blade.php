<section class="col-md-12">

		<?php $count = 0 ?>

		@foreach($posts_category as $post)

			<?php $count = $count + 1 ?>

			@if($count == 1)
				<div class="row post-destaque">
					<a href="{{ route('home.post', $post->id) }}" class="col-md-12 item-photo-grande item fundo-preto" >
						<img class="banner-post img-responsive" src="{{$post->photo->file}}">
						<div class="desc-item-post col-md-12">
							<h6>{{$post->created_at->diffForHumans()}}</h6>
							<h4>{{$post->title}}</h4>
						</div>	
					</a>				
				</div>	

			@endif

		@endforeach
	</section>

	<section class="col-md-12">
		<div class="row">
			<div class="row">
		
				<?php $count = 0 ?>

				@foreach($posts_category as $post)

					<?php $count = $count + 1 ?>

					@if($count > 1)

						<div class="col-md-6">
							<a href="{{ route('home.post', $post->id) }}" class="col-md-12 item-photo-pequena item fundo-preto" >
								<img class="banner-post img-responsive" src="{{$post->photo->file}}">
								<div class="desc-item-post col-md-12">
									<h6>{{$post->created_at->diffForHumans()}}</h6>
									<h4>{{$post->title}}</h4>
								</div>	
							</a>										
						</div>	

					@endif

				@endforeach

				<?php 

				if($count == 0){
					echo "<div class='col-md-6'>";
						echo "<p>Nenhum resultado encontrado!</p>";
					echo "</div>";
				}

			?>

			</div>	
		</div>	
		<div class="col-sm-8 col-sm-offset-2">
			{{$posts_category->render()}}
		</div>