<?php
	use App\Post;
	use App\ViewsPost;

	$posts = Post::withCount('views_post')->orderByDesc('views_post_count')->limit(5)->get();
?>
	<div class="col-md-12" style="margin-bottom: 20px;">
		<h3 class="text-center">Mais Visualizados</h3>
		@foreach($posts as $postagem)
		
			<a href="{{ route('home.post', $postagem->id) }}" class="col-md-12 item-photo-pequena item fundo-preto" >
				<img class="banner-post img-responsive" src="{{$postagem->photo->file}}">
				<div class="desc-item-post col-md-12">
					<h4>{{$postagem->category->name}} <small>{{$postagem->created_at->diffForHumans()}}</small></h4>	
					<h4>{{$postagem->title}}</h4>
				</div>	
			</a>

		@endforeach
	</div>	