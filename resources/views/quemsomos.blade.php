@extends('layouts.blog-home')

@section('content')
	@foreach($empresa as $desc)	

	<section>
		
		<div class="container">
			
			<div class="col-md-8">

				@if(isset($desc->banner))
				<div class="row">
					<img class="img-responsive banner-post" src="{{$desc->banner ? $desc->banner : 'http://placehold.it/400x400'}}">	
				</div>
				@endif
				
				
				<div class="row">
					<h1>Objetivos</h1>
					<br>
					<h4 class="texto"><?=$desc->objetivo?></h4>
				</div>

				<div class="row">
					<h1>História</h1>
					<br>
					<h4 class="texto"><?=$desc->historia?></h4>
				</div>
					
			</div>
			<div class="col-md-4">
				@include('include.user_admin')
			</div>	

		</div>

	</section>

	@endforeach

@endsection
